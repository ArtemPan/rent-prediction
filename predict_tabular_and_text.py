import argparse
from xgboost import XGBRegressor
import pandas as pd
import numpy as np
import joblib

def main(args):

    #read csv
    df = pd.read_csv(args.input_file, lineterminator='\n')
    df_ = df.drop(columns='totalRent')

    #Processing and Features Pipeline
    pipeline = joblib.load(args.pipeline_path)
    transformed_df = pipeline.transform(df_)

    #Load reg model
    xgb_model = XGBRegressor()
    xgb_model.load_model(args.model_path)

    #predict
    pred_y = xgb_model.predict(transformed_df)

    #make submission and save data
    print("Results Submited")
    submission_ = df[['totalRent']]

    submission = pd.merge(submission_,
                          pd.DataFrame(np.exp(pred_y), columns=['pred_totalRent']),
                          left_index=True,
                          right_index=True,
                          how='left')

    submission.to_csv('data/submission_.csv', index=False)

if __name__ == '__main__':
    # read parameters
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_path',
                        help='Path to the model file.',
                        default="saved_models/XGB/XGBRegressor_json_data_5.json_2022-04-14 23:56:36.json")
    parser.add_argument('--pipeline_path',
                        help='Path to the  pipelines weight',
                        default="saved_models/Pipeline/pipeline_json_data_5.json_2022-04-14 23:56:36.pkl")
    parser.add_argument('--input_file',
                        help='Path to the evalset file',
                        default='./data/validation.csv')

    args = parser.parse_args()
    main(args)