from xgboost import XGBRegressor
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import MiniBatchKMeans
from sklearn.metrics import silhouette_score
import pickle
import ploting

class XGB():

    def __init__(self, X_train, X_valid, Y_train, Y_valid, find_parameters):
        self._X_train = X_train
        self._X_valid = X_valid
        self._Y_train = Y_train
        self._Y_valid = Y_valid
        self._find_parameters = find_parameters

    def train(self):
        if self._find_parameters == True:
            learning_rate, n_estimators = self._search_parametrs()
            print("n_estimators:", n_estimators)
            print("learning_rate:", learning_rate)
            best_model = XGBRegressor(n_estimators=n_estimators,
                                      learning_rate=learning_rate)
            print("""Train XGB""")
            best_model.fit(self._X_train, self._Y_train,
                       eval_set=[(self._X_train, self._Y_train), (self._X_valid, self._Y_valid)],
                       eval_metric='rmsle')
            self._scores(self._Y_train, best_model.predict(self._X_train), 'train')
            print(""" """)
            self._scores(self._Y_valid, best_model.predict(self._X_valid), 'valid')
        else:
            best_model = XGBRegressor(n_estimators=800,
                                      learning_rate=0.2,
                                      max_depth=10)
            print("""Train XGB""")
            best_model.fit(self._X_train, self._Y_train,
                           eval_set=[(self._X_train, self._Y_train), (self._X_valid, self._Y_valid)],
                           eval_metric='rmsle'
                           )

            self._scores(self._Y_train, best_model.predict(self._X_train), 'train')
            print(""" """)
            self._scores(self._Y_valid,best_model.predict(self._X_valid), 'valid')

        return best_model

    def _search_parametrs(self):

        model = XGBRegressor(n_estimators=250, learning_rate=0.03)

        my_pipeline = Pipeline(steps=[
            ('model', model)
        ])
        params = {'model__n_estimators': [400, 425, 450, 475, 500, 600, 700, 800],
                  'model__learning_rate': [0.001, 0.1, 0.2, 0.3, 0.6, 0.03, 0.06, 0.09]}

        search_model = GridSearchCV(my_pipeline, param_grid=params)

        print("""Searching best parameters of XGB""")
        search_model.fit(self._X_train, self._Y_train)

        learning_rate, n_estimators = search_model.best_params_.values()

        return learning_rate, n_estimators

    def _scores(self, y, yp, type):

        mse = mean_squared_error(y, yp)
        print("MSE_{0}: %.2f".format(type) %mse)
        print("RMSE_{0}: %.2f".format(type) % (mse ** (1 / 2.0)))
        r2 = r2_score(y, yp)
        print('r2_{0}:%.2f'.format(type) %r2)


class Tfidf():
    def __init__(self, corpus):
        self._corpus = corpus

    def get_corpus(self):
        vectorizer = TfidfVectorizer()
        text = vectorizer.fit_transform(self._corpus)
        save = self._save_corpus(vectorizer)

        return text

    def _save_corpus(self, _vectorizer):
        pickle.dump(_vectorizer, open("saved_models/Tfidf/TfidfVector.pkl", "wb"))

class KMeans():

    def __init__(self, text, manual_order):
        self._text = text
        self._manual_order = manual_order

    def fit_kmeans(self):
        if self._manual_order == False:
            print("Finding opt_n_clusters >>>>")
            opt_n_cluster = self._find_optimal_clusters(self._text, 35)
            print("opt_n_cluster:", opt_n_cluster)
            clt = MiniBatchKMeans(n_clusters=opt_n_cluster, init_size=1024, batch_size=2048, random_state=20).fit(self._text)
            save = self._save_model(clt)
        else:
            ploting.find_optimal_clusters(self._text, 35)
            try:
                opt_n_cluster = int(input("input number of clusters: "))
            except:
                print("Error, not an integer entered")

            clt = MiniBatchKMeans(n_clusters=opt_n_cluster, init_size=1024, batch_size=2048, random_state=20).fit(self._text)
            clusters = clt.predict(self._text)
            ploting.plot_tsne_pca(self._text, clusters)
            save = self._save_model(clt)


    def _save_model(self, _clt):
        pickle.dump(_clt, open("saved_models/MiniBatchKMeans/MiniBatchKMeans.pkl", "wb"))

    def _find_optimal_clusters(self, text, max_k):

        silhouette_avg = []
        range_n_clusters = []

        for num_clusters in range(2, max_k + 1, 1):
            # initialise kmeans
            kmeans = MiniBatchKMeans(n_clusters=num_clusters)
            kmeans.fit(text)
            cluster_labels = kmeans.labels_
            # silhouette score
            range_n_clusters.append(num_clusters)
            silhouette_avg.append(silhouette_score(text, cluster_labels))

        opt_n_cluster = range_n_clusters[silhouette_avg.index(max(silhouette_avg))]

        return opt_n_cluster























