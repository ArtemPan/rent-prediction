import argparse
import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
import json
import joblib
import datetime
from xgboost import XGBRegressor
from processing import NAmappingImputer, PandasSimpleImputer, SetDtypes, Outlier_Identification
from processing import SetTarget, LabelEncode, NumEncode, RemoveData, SetTextEmb
from algorithms import XGB
from ploting import plot_viz, error_distribution_viz
from gensim.models import fasttext

def main(args):

    #Load features json
    with open(args.features_json) as json_file:
        json_data = json.load(json_file)

    #Load fasttext model
    print('Loading FastText model......>>>')
    loaded_model = fasttext.load_facebook_vectors('saved_models/Fasttext/model.bin')

    #Load dataframe
    df_ = pd.read_csv(args.input_file, lineterminator='\n')

    #Set target
    pipeline_target = Pipeline(steps=[
        ('set_target', SetTarget(['totalRent'], 'log')),
        ("remove_data", RemoveData(['totalRent']))
    ])
    DF = pipeline_target.transform(df_)

    #Split data
    train_, test_ = train_test_split(DF, test_size=0.20, random_state=42)
    train_, test_ = train_.reset_index(), test_.reset_index()
    train_, test_ = train_.drop(columns='index'), test_.drop(columns='index')

    #Prepare data for train
    Y_train, Y_valid = train_[['y']], test_[['y']]
    train_, test_ = train_.drop(columns='y'), test_.drop(columns='y')

    #Fit processing and features Pipeline
    print('Processing data.............>>>')
    pipe = Pipeline(steps=[
        ('na_mapping', NAmappingImputer(json_data)),
        ('imputer', PandasSimpleImputer(strategy='most_frequent')),
        ('set_d_types', SetDtypes(json_data)),
        ("label_encoder", LabelEncode(json_data['features']['bool'])),
        ("label_cat", LabelEncode(json_data['features']['cat'])),
        ("num_encoder", NumEncode(json_data['features']['num'], 'Robust')),
        ("text_encoder", SetTextEmb(json_data['features']['text'][0], loaded_model)),
        ("remove_data", RemoveData(json_data['features']['drop_col']))
    ])
    pipe.fit(train_)

    #Get name models
    name_models = [args.features_json.split('/')[1],
                   datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")]

    #Save Pipeline dump
    pipename = 'saved_models/Pipeline/pipeline_{0}_{1}.pkl'.format(name_models[0],
                                                                   name_models[1])
    joblib.dump(pipe, pipename)

    #Transform data
    X_train = pipe.transform(train_)
    X_valid = pipe.transform(test_)

    #Remove outliers
    full = pd.merge(left=X_train, right=Y_train, left_index=True, right_index=True)
    X_train, Y_train = Outlier_Identification(full)

    #Training model
    print('Training model...............>>>')
    model_training = XGB(X_train, X_valid, Y_train, Y_valid, args.find_parameters).train()

    #save models
    name_model = "saved_models/XGB/XGBRegressor_{0}_{1}.json".format(name_models[0],
                                                                     name_models[1])
    model_training.save_model(name_model)

    #testing prediction
    pred_y_valid, pred_y_train = _test_prediction(name_model, X_valid, X_train)

    #Save main plots
    _viz_plots(Y_valid, Y_train, pred_y_valid, pred_y_train)

def _test_prediction(_name_model, _X_valid, _X_train):

    xgb_model = XGBRegressor()
    xgb_model.load_model(_name_model)
    pred_y_valid = xgb_model.predict(_X_valid)
    pred_y_train = xgb_model.predict(_X_train)

    return pred_y_valid, pred_y_train

def _viz_plots(_Y_valid, _Y_train, _pred_y_valid, _pred_y_train):
    # save and viz main plots
    plot_viz(_Y_valid, _pred_y_valid, 200, 'Actual and predicted price')
    error_distribution_viz(_Y_train, _pred_y_train, 'Train data')
    error_distribution_viz(_Y_valid, _pred_y_valid, 'Validate data')

if __name__ == '__main__':
    # read parameters
    parser = argparse.ArgumentParser()
    parser.add_argument('--features_json',
                        help='Path to the json file',
                        default="processing_jsons/json_data_5.json"
                        )
    parser.add_argument('--input_file',
                        help='Path to the main file',
                        default='data/train.csv')
    parser.add_argument('--find_parameters',
                        help='True for finding best parameters by Grid Search in XGB',
                        default=False)

    args = parser.parse_args()
    main(args)