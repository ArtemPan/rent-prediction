# Rent Prediction

## Project Description

The objective is to train and predict the rent by the data (`train.csv`,`test.csv`).

Find models data in zip file (send by email)

## Installation
The following command installs all necessary packages:

```.bash

conda create --name rent_prediction python=3.7
conda activate rent_prediction
pip install -r requirements.txt

```

The project was used Anaconda package and tested using Python 3.7.

## Datasets
All  datasets could find in projects folders (data). 

## Train model
To train the model (tabular data with and without text data), simply run:

```.bash
python train_tabular.py --find_parameters [False]
python train_tabular_and_text.py --find_parameters [False]

```

Or use predefined parameters

```.bash
python train_tabular.py
python train_tabular_and_text.py
```

There are a lot of parameters to specify among them:
  
- `find_parameters` the search of best GXB parameters, True - searching or False - predifine;
- `'features_json` the dictionary where exists information how processing data

In our experiments we had 80/20 train/test split on training stage.

## Model inference
To run your model on the input file use the following command:

```.bash

python predict_tabular.py --model_path MODEL_PATH --pipeline_path PIPELINE_PATH  --input_file INPUT_FILE_PATH
python predict_tabular_and_text.py --model_path MODEL_PATH --pipeline_path PIPELINE_PATH  --input_file INPUT_FILE_PATH                  
```
Or use predefined parameters
```.bash
python predict_tabular.py
python predict_tabular_and_text.py

```
## Additional

```.bash
You could find more information as data analysis, features processing or engineering, modeling,
and benchmarks of the algorithms in the research notebook. 
```
## Evaluation

Train with without text data 
```
MSE_train: 0.01
RMSE_train: 0.03
r2_train:0.98
 
MSE_valid: 0.04
RMSE_valid: 0.21
r2_valid:0.85
```

Train with text data (use only 'description' columns)
```
MSE_train: 0.01
RMSE_train: 0.08
r2_train:0.98
 
MSE_valid: 0.04
RMSE_valid: 0.19
r2_valid:0.88
```

Train with text data (use only 'facilities' and 'description' columns)
```
MSE_train: 0.01
RMSE_train: 0.07
r2_train:0.98
 
MSE_valid: 0.03
RMSE_valid: 0.18
r2_valid:0.88
```
Moreover, please find visualization of predict vs target results in folders plots

## Improvment

```.bash
- Add polynomial or interaction Features because effecting to R2 and MSE value;
- Add early stoping;
- Use cross validation in tranings;
- Add logs;
- Improve code structure;
- Add a more accurately description in code; 
- Compare results with automl approaches (GCP or H2o);
- Testing different feature processing approaches;
- Find a more accurate intersection between text and price; 
- Testing additional modeling (Deep Learning, change Stacked Ensemble Models);
- Improve grid search for the XGB and Stacked Ensemble Models; 
- Fix in skewed approach in notebook (NA in livingSpace, noRooms, serviceCharge);
- Features engineering: find сentre of city, most popular and rare streets;
- Testing different processing methods of text: use main phrase, find so frequensy words and etc;
- Fix MSE in validation prediction and analysis mistake according to region;
- make traning and predict according to region only. 
```