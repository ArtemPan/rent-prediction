import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import statsmodels.api as sm
from sklearn.cluster import MiniBatchKMeans
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import matplotlib.cm as cm

def plot_viz(Y, Y_pred, random_size, title):

    idx = np.random.choice(range(Y.shape[0]), size=random_size, replace=False)
    Y_ = np.array(Y)[idx]
    Y_pred_ = Y_pred[idx]
    x_ax = range(len(Y_))
    plt.plot(x_ax, Y_, label="original")
    plt.plot(x_ax, Y_pred_, label="predicted")
    plt.title(title)
    plt.legend()
    plt.savefig("plots/predict_target.jpg")
    plt.show()

def error_distribution_viz(Y, Y_pred, title):

    error_dist = pd.Series((Y_pred - np.array(Y.y)))
    fig = plt.figure(figsize=(10, 10))
    plt.subplot(221)
    error_dist.plot(kind='hist', bins=50)
    ax = plt.subplot(222)
    sm.qqplot(error_dist, line='s', ax=ax)
    plt.title(title)
    plt.savefig("plots/predict_{}_data.jpg".format(title))
    plt.show()

def find_optimal_clusters(data, max_k):
    iters = range(2, max_k + 1, 2)

    sse = []
    for k in iters:
        sse.append(MiniBatchKMeans(n_clusters=k, init_size=1024, batch_size=2048, random_state=20).fit(data).inertia_)
        print('Fit {} clusters'.format(k))

    f, ax = plt.subplots(1, 1)
    ax.plot(iters, sse, marker='o')
    ax.set_xlabel('Cluster Centers')
    ax.set_xticks(iters)
    ax.set_xticklabels(iters)
    ax.set_ylabel('SSE')
    ax.set_title('SSE by Cluster Center Plot')
    plt.savefig("plots/find_clusters.jpg")
    plt.show()

def plot_tsne_pca(data, labels):
    max_label = max(labels)
    max_items = np.random.choice(range(data.shape[0]), size=500, replace=False)

    pca = PCA(n_components=2).fit_transform(data[max_items, :].todense())
    tsne = TSNE().fit_transform(PCA(n_components=50).fit_transform(data[max_items, :].todense()))

    idx = np.random.choice(range(pca.shape[0]), size=500, replace=False)
    label_subset = labels[max_items]
    label_subset = [cm.hsv(i / max_label) for i in label_subset[idx]]

    f, ax = plt.subplots(1, 2, figsize=(14, 6))

    ax[0].scatter(pca[idx, 0], pca[idx, 1], c=label_subset)
    ax[0].set_title('PCA Cluster Plot')

    ax[1].scatter(tsne[idx, 0], tsne[idx, 1], c=label_subset)
    ax[1].set_title('TSNE Cluster Plot')
    plt.savefig("plots/clusters.jpg")
    plt.show()

