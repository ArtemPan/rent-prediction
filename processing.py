from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import MaxAbsScaler, LabelEncoder, RobustScaler, MinMaxScaler, OneHotEncoder
from sklearn.exceptions import NotFittedError
from sklearn.impute import SimpleImputer
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.snowball import SnowballStemmer
import string
from sklearn import preprocessing
stemmer = SnowballStemmer("german")


def CreateTrainValidDF(df):

    Y = df[['y']]
    X = df.drop(columns='y')

    X_,Y_ = train_test_split(X, Y, test_size=0.2, random_state=42)

    return X_, Y_

def Outlier_Identification(df):

    linear = LinearRegression()
    Y = df['y']
    linear.fit(df.drop(['y'], axis=1), Y)
    Y_hat = linear.predict(df.drop(['y'], axis=1))
    residuals = Y - Y_hat
    y_vs_yhat_df = pd.DataFrame(zip(Y.values, Y_hat, residuals), columns=['y', 'yhat', 'residuals'], index=df.index)
    standard_residuals = (residuals - residuals.mean()) / residuals.std()
    outliers = df[abs(standard_residuals) > 3]
    y_vs_yhat_df.loc[y_vs_yhat_df.index.isin(outliers.index), 'Outlier'] = 1
    y_vs_yhat_df.loc[y_vs_yhat_df['Outlier'] != 1, 'Outlier'] = 0

    X_with_out_outlier = df.loc[y_vs_yhat_df[y_vs_yhat_df['Outlier'] == 0].index, :]

    X_train_, Y_train_ = X_with_out_outlier.drop(['y'], axis=1), X_with_out_outlier[['y']]

    return X_train_, Y_train_


class PandasSimpleImputer(SimpleImputer):
    """A wrapper around `SimpleImputer` to return data frames with columns."""

    def fit(self, X, y=None):
        self.columns = X.columns
        return super().fit(X, y)

    def transform(self, X):
        df = pd.DataFrame(super().transform(X), columns=self.columns)

        return df


class NAmappingImputer(BaseEstimator, TransformerMixin):
    """NA processing and return maping data to frames with columns."""

    def __init__(self, js_features):
        self._js_features = js_features

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        for col_name, value in self._js_features['na_map'].items():
            if type(value) == str:
                X[col_name] = X[col_name].fillna(str(value))
            else:
                X[col_name] = X[col_name].fillna(value)

        return X


class SetDtypes(BaseEstimator, TransformerMixin):
    """Set datatypes for columns"""

    def __init__(self, js_features=dict):
        self._js_features = js_features

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        for type_feature in self._js_features['preprocessing'].keys():
            if type_feature == 'date':
                for col in self._js_features['preprocessing']['date']:
                    col_name_new = col + '_Y'
                    X[col_name_new] = pd.to_datetime(X[col], format='%Y%m%d') # Set date datatype
                    X[col_name_new] = pd.DatetimeIndex(X[col_name_new]).year

            elif type_feature == 'str':
                for col in self._js_features['preprocessing']['str']:
                    X[col] = X[col].astype('str')

            elif type_feature == 'int':
                for col in self._js_features['preprocessing']['int']:
                    print(col)
                    X[col] = X[col].astype('int')

            elif type_feature == 'float':
                for col in self._js_features['preprocessing']['float']:
                    X[col] = X[col].astype('float')
        return X

class SetTarget(BaseEstimator, TransformerMixin):
    """Set Target column"""

    def __init__(self, cols=[], type=str):
        self._cols = cols
        self._type = type

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        if self._type == 'log':
            X['y'] = np.log(np.array(X[self._cols]))
        else:
            X['y'] = X[self._cols]

        return X

class SetTextEmb(BaseEstimator, TransformerMixin):
    def __init__(self, col_name, ft_model):
        self._col_name = col_name
        self._ft_model = ft_model

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):

        review_lines = self._text_clean_tokenize(X[self._col_name])
        emb_df = pd.DataFrame([self._sentence_vector(x, self._ft_model) for x in review_lines],
                               columns=[str(x) + '_'+ str(self._col_name) for x in range(0, self._ft_model.vector_size)])

        X = pd.merge(X, pd.DataFrame(emb_df),
                     how='left', left_index=True, right_index=True)

        return X

    def _sentence_vector(self, list_words, _ft_model):

        if (len(list_words) == 1) or (len(list_words) == 0) :
            sentence = ['keine information']
        else:
            sentence = list_words

        vecs = [_ft_model[x] for x in sentence]
        X = np.asarray(vecs, dtype=np.float)  # Float is needed.
        X_normalized = preprocessing.normalize(X, norm='l2')  # l2-normalize the samples (rows).
        return np.mean(X_normalized, axis=0)


    def _text_clean_tokenize(self, data):

        review_lines = list()
        lines = data.values.astype(str).tolist()
        for line in lines:
            tokens = word_tokenize(line)
            tokens = [w.lower() for w in tokens]
            table = str.maketrans('', '', string.punctuation)
            stripped = [w.translate(table) for w in tokens]
            # remove remaining tokens that are not alphabetic
            words = [word for word in stripped if word.isalpha()]
            stop_words = set(stopwords.words('german'))
            words = [w for w in words if not w in stop_words]
            words = [stemmer.stem(w) for w in words]
            review_lines.append(words)

        return (review_lines)


class SetTextLabels(BaseEstimator, TransformerMixin):
    """Create Labels from text"""

    def __init__(self, model, vectorizer, col='str'):
        self._model = model
        self._vectorizer = vectorizer
        self._col = col

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        corpus = list(X[self._col])
        text = self._vectorizer.transform(corpus)
        clusters = self._model.predict(text)
        X = pd.merge(X,
                     pd.DataFrame(clusters).rename(columns={0: 'label'}),
                     how='left', left_index=True, right_index=True)
        return X


class CustomMapping(BaseEstimator, TransformerMixin):
    """Create Labels from mapping features"""

    def __init__(self, js_mapping, js_col_name):
        self._js_mapping = js_mapping
        self._js_col_name = js_col_name

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        for _col_name in self._js_col_name:
            col_name_new = _col_name + '_transform'
            X[col_name_new] = X[_col_name].map(dict(self._js_mapping[_col_name]))

        return X


class LabelEncode(BaseEstimator, TransformerMixin):
    """Сreate Labels from binary features"""

    def __init__(self, cols=[]):
        self._cols = cols

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        # Label
        En = LabelEncoder()
        for col in X[self._cols]:
            col_name_new = col + '_transform'
            X[col_name_new] = En.fit_transform(X[col])

        return X

class NumEncode(BaseEstimator, TransformerMixin):
    """Create Numeric features"""

    def __init__(self, cols=[], type_skaler=str):
        self._cols = cols
        self._type_skaler = type_skaler

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        # Encode
        if self._type_skaler == 'MaxAbs':
            scaler = MaxAbsScaler()
            DF = self._df(X, scaler)

        elif self._type_skaler == 'Robust':
            scaler = RobustScaler()
            DF = self._df(X, scaler)
        elif self._type_skaler == 'MinMax':
            scaler = MinMaxScaler()
            DF = self._df(X, scaler)

        return DF

    def _df(self, X, scaler):
        for col in X[self._cols]:
            col_name_new = col + '_transform'
            X[col_name_new] = pd.DataFrame(scaler.fit_transform(X[[col]]))

        return X


class DataFrameOneHotEncoder(BaseEstimator, TransformerMixin):
    """Specialized version of OneHotEncoder that plays nice with pandas DataFrames and
    will automatically set the feature/column names after fit/transform
    """

    def __init__(
            self,
            categories="auto",
            drop=None,
            sparse=None,
            dtype=np.float64,
            handle_unknown="error",
            col_overrule_params={},
    ):
        """Create DataFrameOneHotEncoder that can be fitted to and transform dataframes
        and that will set up the column/feature names automatically to
        original_column_name[categorical_value]
        If you provide the same arguments as you would for the sklearn
        OneHotEncoder, these parameters will apply for all of the columns. If you want
        to have specific overrides for some of the columns, provide these in the dict
        argument col_overrule_params.

        For example:
            DataFrameOneHotEncoder(col_overrule_params={"col2":{"drop":"first"}})
        will create a OneHotEncoder for each of the columns with default values, but
        uses a drop=first argument for columns with the name col2
        Args:
            categories‘auto’ or a list of array-like, default=’auto’
                ‘auto’ : Determine categories automatically from the training data.
                list : categories[i] holds the categories expected in the ith column.
                The passed categories should not mix strings and numeric values
                within a single feature, and should be sorted in case of numeric
                values.
            drop: {‘first’, ‘if_binary’} or a array-like of shape (n_features,),
                default=None
                See OneHotEncoder documentation
            sparse: Ignored, since we always will work with dense dataframes
            dtype: number type, default=float
                Desired dtype of output.
            handle_unknown: {‘error’, ‘ignore’}, default=’error’
                Whether to raise an error or ignore if an unknown categorical feature
                is present during transform (default is to raise). When this parameter
                is set to ‘ignore’ and an unknown category is encountered during
                transform, the resulting one-hot encoded columns for this feature will
                be all zeros. In the inverse transform, an unknown category will be
                denoted as None.
            col_overrule_params: dict of {column_name: dict_params} where dict_params
                are exactly the options cateogires,drop,sparse,dtype,handle_unknown.
                For the column given by the key, these values will overrule the default
                parameters
        """
        self.categories = categories
        self.drop = drop
        self.sparse = sparse
        self.dtype = dtype
        self.handle_unknown = handle_unknown
        self.col_overrule_params = col_overrule_params
        pass

    def fit(self, X, y=None):
        """Fit a separate OneHotEncoder for each of the columns in the dataframe
        Args:
            X: dataframe
            y: None, ignored. This parameter exists only for compatibility with
                Pipeline
        Returns
            self
        Raises
            TypeError if X is not of type DataFrame
        """
        if type(X) != pd.DataFrame:
            raise TypeError(f"X should be of type dataframe, not {type(X)}")

        self.onehotencoders_ = []
        self.column_names_ = []

        for c in X.columns:
            # Construct the OHE parameters using the arguments
            ohe_params = {
                "categories": self.categories,
                "drop": self.drop,
                "sparse": False,
                "dtype": self.dtype,
                "handle_unknown": self.handle_unknown,
            }
            # and update it with potential overrule parameters for the current column
            ohe_params.update(self.col_overrule_params.get(c, {}))

            # Regardless of how we got the parameters, make sure we always set the
            # sparsity to False
            ohe_params["sparse"] = False

            # Now create, fit, and store the onehotencoder for current column c
            ohe = OneHotEncoder(**ohe_params)
            self.onehotencoders_.append(ohe.fit(X.loc[:, [c]]))

            # Get the feature names and replace each x0_ with empty and after that
            # surround the categorical value with [] and prefix it with the original
            # column name
            feature_names = ohe.get_feature_names()
            feature_names = [x.replace("x0_", "") for x in feature_names]
            feature_names = [f"{c}[{x}]" for x in feature_names]

            self.column_names_.append(feature_names)

        return self

    def transform(self, X):
        """Transform X using the one-hot-encoding per column
        Args:
            X: Dataframe that is to be one hot encoded
        Returns:
            Dataframe with onehotencoded data
        Raises
            NotFittedError if the transformer is not yet fitted
            TypeError if X is not of type DataFrame
        """
        if type(X) != pd.DataFrame:
            raise TypeError(f"X should be of type dataframe, not {type(X)}")

        if not hasattr(self, "onehotencoders_"):
            raise NotFittedError(f"{type(self).__name__} is not fitted")

        all_df = []

        for i, c in enumerate(X.columns):
            ohe = self.onehotencoders_[i]

            transformed_col = ohe.transform(X.loc[:, [c]])

            df_col = pd.DataFrame(transformed_col, columns=self.column_names_[i])
            all_df.append(df_col)

        return pd.concat(all_df, axis=1)


class RemoveData(BaseEstimator, TransformerMixin):
    """Remove unnecesseary columns"""

    def __init__(self, drop_cols=[]):
        self._drop_cols = drop_cols

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        # Drop unneeded columns (many null values)
        X = X.drop(columns=self._drop_cols)

        return X
